variable "instance_count" {
  type = map(string)
  default = {
    DEV = 1

  }
}

variable "ami_id" {
  type = map(string)
  default = {
    DEV = "ami-090fa75af13c156b4"

  }
}

variable "instance_type" {
  type = map(string)
  default = {
    DEV = "t2.micro"
  }
}

variable "subnet_id" {
  type = map(string)
  default = {
    DEV = "subnet-0c00bad1ccb2c7ea5"

  }
}

variable "security_groups" {
  type = map(list(string))
  default = {
    DEV = ["sg-00582e1982a2715dd"]

  }
}


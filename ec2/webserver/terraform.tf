
terraform {
  required_version = "1.2.6"
  backend "s3" {
    bucket         = "amol-terraform-statelatest"
    key            = "ec2/wbeserver.tfstate"
    region         = "us-east-1"
    dynamodb_table = "erraform_state"
  }
}

